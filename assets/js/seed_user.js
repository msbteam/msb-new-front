$(function () {
    $('#seedUser').click( function (e) {
        e.preventDefault();

        $('#myModal').modal('show');
        $('#myModal .dataframe').attr('src','https://bringtheblog.com/cron/seed_blogs.php?id=' + $('#seedUser').attr('data-id') + '&m=' + $('#seed_user_length').val());
    });
});

