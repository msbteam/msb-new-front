var task_event_list = [
    {"task_id": 1, "task_name": "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium"},
    {"task_id": 2, "task_name": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. "},
    {"task_id": 3, "task_name": "Praesent mauris. Fusce nec tellus sed augue semper porta."},
    {"task_id": 4, "task_name": "Sed dignissim lacinia nunc. Curabitur tortor. Pellentesque nibh. Aenean quam."},
    {"task_id": 5, "task_name": "Morbi lacinia molestie dui. Praesent blandit dolor. Sed non quam. In vel mi sit amet augue congue elementum."},
    {"task_id": 6, "task_name": "Ut ultrices ultrices enim. Curabitur sit amet mauris. Morbi in dui quis est pulvinar ullamcorper. "},
    {"task_id": 7, "task_name": "Maecenas mattis. Sed convallis tristique sem. Proin ut ligula vel nunc egestas porttitor."},
    {"task_id": 8, "task_name": "Morbi lectus risus, iaculis vel, suscipit quis, luctus non, massa. Fusce ac turpis quis ligula lacinia aliquet."},
    {"task_id": 9, "task_name": "Sed lacinia, urna non tincidunt mattis, tortor neque adipiscing diam, a cursus ipsum ante quis turpis."},
    {"task_id": 10, "task_name": "Sed lacinia, urna non tincidunt mattis, tortor neque adipiscing diam, a cursus ipsum ante quis turpis."}
]

var wizard_steps = [
    {"main_title": "Choose a Category", "wizardStep": 1, "step_title": "Select Your Event Category", "step_description": "Select one of the categories below to filter available tasks. Tip: Use the search bar on the right to type in keywords and quickly find an event.", "buttonName": "Select a Category"},
    {"main_title": "Select Subcategory", "wizardStep": 1, "step_title": "Select Your Subcategory", "step_description": "Select one of the subcategories below to filter available tasks. You can use your arrow keys to navigate back and forth between screens.", "buttonName": "Select a Subcategory"},
    {"main_title": "Legend", "wizardStep": 2, "step_title": "Tasks Available for Legend", "step_description": "Select one of the tasks below to create an instance of this task, search within this category using the search bar on the right, or choose a different category.", "buttonName": null},
    {"main_title": "Complete Event - Step 1", "wizardStep": 3, "step_title": "Event Step 1", "step_description": "Task: Nam libero tempore, soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime", "buttonName": "Next"},
    {"main_title": "Complete Event - Step 2", "wizardStep": 3, "step_title": "Event Step 2", "step_description": "Task: Nam libero tempore, soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime", "buttonName": "Next"},
    {"main_title": "Complete Event - Step 3", "wizardStep": 3, "step_title": "Event Step 3", "step_description": "Task: Nam libero tempore, soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime", "buttonName": "Next"},
    {"main_title": "Complete Event - Step 4", "wizardStep": 3, "step_title": "Event Step 4", "step_description": "Task: Nam libero tempore, soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime", "buttonName": "Next"},
    {"main_title": "Task Complete", "wizardStep": 4, "step_title": "Congratulations on completing the task! Review or edit your tasks below.", "step_description": "Task: Nam libero tempore, soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime", "buttonName": "Next"},
    {"main_title": "Task Stats", "wizardStep": 5, "step_title": "Task Name", "step_description": "Nam libero tempore, soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime", "buttonName": "Next"}
];

var task_list_main = [
    {
        "name": "Pharmacy Access",
        "showNow": true,
        "categoryType": "pharmacy",
        "subCategories": [
            {
                "name": "Real Subcategory",
                "categoryType": "block",
                "task": "task-list",
                "description": "Suspendisse."
            },
            {
                "name": "Subtask 2",
                "categoryType": "block",
                "task": "empty"
            }
        ],
        "description": "<p>Medication management is a category with four subcategories and contains any tasks having to do with:</p> <p>Legend<br> Controlled Substances<br> Over-The-Counter<br> Controlled substances<br> Non-Sterile Compounding USP</p>"
    },
    {
        "name": "Patient Care",
        "showNow": false,
        "categoryType": "patient",
        "subCategories": [
            {
                "name": "Subtask 3",
                "categoryType": "block",
                "task": "empty",
                showNow: true,
                "description": "Aliquam nibh felis, facilisis vitae ullamcorper viverra, placerat ac felis. Cras suscipit sagittis felis. Maecenas fringilla dolor a lorem blandit consectetur."
            },
            {
                "name": "Breach 4",
                "categoryType": "block",
                "task": "empty",
                "description": "Molestie, erat quis dictum viverra, ligula turpis faucibus mauris, at euismod elit erat a lectus.."
            },
            {
                "name": "Subtask 5",
                "categoryType": "block",
                "task": "empty",
                "description": "Sed ornare tempor justo, non molestie velit auctor nec. Nulla augue velit, porttitor nec augue a, pellentesque pretium urna.."
            },
            {
                "name": "Main task 6",
                "categoryType": "block",
                "task": "empty",
                "description": "Ut vel rutrum mauris, sit amet dignissim risus. Maecenas at diam vitae velit lobortis gravida sed in eros.."
            }
        ],
        "description": "Suspendisse molestie, erat quis dictum viverra, ligula turpis faucibus mauris, at euismod elit erat a lectus. Fusce a sollicitudin tortor. Integer rhoncus arcu vel nibh suscipit posuere. Aliquam condimentum elit ut purus dapibus, id lobortis elit maximus. Quisque lectus eros, facilisis sit amet bibendum sed, mollis et leo. Mauris consequat eget dolor nec accumsan. Vestibulum viverra vitae neque eu mattis."
    },
    {
        "name": "Licensing / Education",
        "showNow": false,
        "categoryType": "education",
        "subCategories": [
            {
                "name": "Subtask 7",
                "categoryType": "block",
                "task": "empty"
            },
            {
                "name": "Snapshot 8",
                "categoryType": "block",
                "task": "empty"
            },
            {
                "name": "Tertiary task 9",
                "categoryType": "block",
                "task": "empty"
            }
        ],
        "description": "Curabitur at cursus velit, quis suscipit lorem. Sed sodales nunc vitae tempus molestie. Aenean laoreet elit quis tempus pharetra. Curabitur commodo mauris vitae mi finibus mollis ut a justo. Vivamus ultricies turpis id ante finibus faucibus."
    },
    {
        "name": "Investigation / Audit",
        "showNow": false,
        "categoryType": "investigation",
        "subCategories": [
            {
                "name": "Subtask 8",
                "categoryType": "block",
                "task": "empty"
            },
            {
                "name": "Snapshot 9",
                "categoryType": "block",
                "task": "empty"
            },
            {
                "name": "Tertiary task 10",
                "categoryType": "block",
                "task": "empty"
            }
        ],
        "description": "Donec euismod venenatis orci vitae lacinia. Aliquam erat volutpat. Fusce egestas quam sed metus tincidunt, vitae tempus dui maximus. Maecenas eleifend, mi eu egestas lacinia, odio ligula ultricies ex, id aliquet sem turpis vel justo. Vestibulum eros elit, tempus in nulla id, consequat gravida nulla."
    },
    {
        "name": "Medication Management",
        "showNow": false,
        "categoryType": "medication",
        "subCategories": [
            {
                "name": "Subtask 9",
                "categoryType": "block",
                "task": "empty"
            },
            {
                "name": "Snapshot 10",
                "categoryType": "block",
                "task": "empty"
            },
            {
                "name": "Tertiary task 11",
                "categoryType": "block",
                "task": "empty"
            }
        ],
        "description": "Suspendisse et tristique metus, non molestie libero. Fusce interdum congue dignissim. Maecenas rutrum pharetra commodo. Suspendisse eros quam, dignissim sit amet orci in, rutrum fermentum urna. Quisque sit amet felis non libero efficitur semper maximus ac neque."
    }
];