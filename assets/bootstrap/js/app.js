(function(){

    function isNumber(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

    var app = angular.module('quicknav', ['ngSanitize']);
    app.controller('WizardController', function($scope, $http) {
        this.taskList = $http.get('assets/js/data/task_data.json').success(function(data) {
            return data;
        });

        this.tasks = task_list_main;
        this.event_tasks = task_event_list;

        this.tab = 1;
        this.templatePath = 'templates/default.html';
        this.wizardTemplate = 'templates/wizard/step-1.html';
        this.taskTitleName = '';
        this.wpInfo = [];
        this.wpInfo.steps = wizard_steps;
        this.wpInfo.currentStep = 0;
        this.wpInfo.categoryIndex = 1;
        this.wpInfo.subCategoryIndex = 0;
        this.wpInfo.eventTaskIndex = 0;
        this.wpInfo.description = this.tasks[0].description;
        this.wpInfo.subCatDesc = this.tasks[1].subCategories[0].description;
        this.wizard = [];
        this.subCategoryActive = '';


        this.addTask = function (template, taskName) {
            this.templatePath = 'templates/' + template + '.html';
            this.taskTitleName = taskName;
        };

        this.startTask = function (taskName) {
            this.templatePath = 'templates/pharmacy-1.html';
        };

        this.wizardBelt = function (template) {
            this.templatePath = 'templates/wizard/' + template + '.html';
        };

        this.reviewMode = function (template) {
            this.templatePath = 'templates/review.html';
        };

        this.submitTask = function (template) {
            this.templatePath = 'templates/task-overview.html';
        };

        this.wizardChangeStep = function (stepChange) {
            if (stepChange == 'forward') {
                if (this.wpInfo.currentStep < this.wpInfo.steps.length)
                    this.wpInfo.currentStep++;
            } else if (stepChange == 'back') {
                if (this.wpInfo.currentStep > 0)
                    this.wpInfo.currentStep--;
            } else {
                if (isNumber(stepChange) && this.wpInfo.currentStep > 0 && this.wpInfo.currentStep < this.wpInfo.steps.length)
                    this.wpInfo.currentStep = stepChange;
            }
        };

        this.wizardPreview = function(scopeIndex) {
            angular.forEach(this.tasks, function (tasks) {
                tasks.showNow = false;
            });

            this.wpInfo.description = this.tasks[scopeIndex].description;
            this.tasks[scopeIndex].showNow = true;
            this.categoryIndex = scopeIndex;
        };

        this.wizardPreviewSub = function(scopeIndex) {
            angular.forEach(this.tasks[this.wpInfo.categoryIndex].subCategories, function (tasks) {
                tasks.showNow = false;
            });

            this.wpInfo.subCategoryIndex = scopeIndex;
            this.tasks[this.wpInfo.categoryIndex].subCategories[scopeIndex].showNow = true;
            this.wpInfo.subCatDesc = this.tasks[this.wpInfo.categoryIndex].subCategories[scopeIndex].description;
        };

        this.addEventTask = function (scopeIndex) {
            this.wpInfo.currentStep++;
            this.wpInfo.eventTaskIndex = scopeIndex;
        };

        this.toggleTaskDisplay = function (show) {
            if (show) {
                angular.forEach(this.tasks, function (tasks) {
                    tasks.showNow = true;
                });
            } else {
                angular.forEach(this.tasks, function (tasks) {
                    tasks.showNow = false;
                });
            }
        };

        this.toggleSectionDisplay = function (scopeIndex, value) {
            value ? this.tasks[scopeIndex].showNow = false : this.tasks[scopeIndex].showNow = true;
        };
    });

})();




