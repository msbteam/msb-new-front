-- ==========================
-- REPLACE STORED PROCEDURES
-- ==========================

---- Update Staging Site

DELIMITER $$
DROP PROCEDURE IF EXISTS wpPostMetaSearch $$
CREATE PROCEDURE wpPostMetaSearch(searchStr TEXT, replaceStr TEXT)
BEGIN
    DECLARE done BOOL;
    DECLARE the_table_name VARCHAR(64);
    DECLARE set_id INT;
    DECLARE curl CURSOR FOR SELECT
                              information_schema.TABLES.TABLE_NAME
                            FROM information_schema.TABLES
                            WHERE
                              information_schema.TABLES.TABLE_SCHEMA = 'mysmartb_wrdp3' AND
                              (information_schema.TABLES.TABLE_NAME LIKE 'wp\___\_options' OR
                              information_schema.TABLES.TABLE_NAME LIKE 'wp\____\_options');

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    BEGIN -- SET ID BLOCK
      DECLARE CONTINUE HANDLER FOR NOT FOUND SET @nothinHere = TRUE;
      SELECT result_set_id INTO set_id FROM mysmartb_msbdev.blog_manage_sp_results ORDER BY id DESC LIMIT 1;

      IF @nothinHere THEN
        SET set_id = 1;
      ELSE
        SET set_id = set_id+1;
      END IF;
    END; -- END SET ID BLOCK

    OPEN curl;

    the_loop: LOOP
      FETCH curl
      INTO the_table_name;

      IF done
      THEN
        LEAVE the_loop;
      END IF;

      SET @replaceCheck = CONCAT('INSERT INTO mysmartb_msbdev.blog_manage_sp_results (SELECT NULL, ', set_id,', ', QUOTE('PostMeta Search'),', ',QUOTE(the_table_name),', ',QUOTE(searchStr),', ',QUOTE(replaceStr),', post_id, meta_key, meta_value, NOW(), meta_id, ', QUOTE('Active'),', NULL FROM mysmartb_wrdp1.', the_table_name,
                                ' WHERE meta_value LIKE ', QUOTE(CONCAT('%', searchStr, '%')),');');

      PREPARE stmt FROM @replaceCheck;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;

    END LOOP the_loop;

    CLOSE curl;
END
$$

CALL wpPostMetaSearch('seven', 'seven')

---- Post Meta Search

DELIMITER $$
DROP PROCEDURE IF EXISTS wpPostMetaSearch $$
CREATE PROCEDURE wpPostMetaSearch(searchStr TEXT, replaceStr TEXT)
BEGIN
    DECLARE done BOOL;
    DECLARE the_table_name VARCHAR(64);
    DECLARE set_id INT;
    DECLARE curl CURSOR FOR SELECT
                              information_schema.TABLES.TABLE_NAME
                            FROM information_schema.TABLES
                            WHERE
                              information_schema.TABLES.TABLE_SCHEMA = 'mysmartb_wrdp1' AND
                              (information_schema.TABLES.TABLE_NAME LIKE 'wp\___\_postmeta' OR
                              information_schema.TABLES.TABLE_NAME LIKE 'wp\____\_postmeta');

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    BEGIN -- SET ID BLOCK
      DECLARE CONTINUE HANDLER FOR NOT FOUND SET @nothinHere = TRUE;
      SELECT result_set_id INTO set_id FROM mysmartb_msbdev.blog_manage_sp_results ORDER BY id DESC LIMIT 1;

      IF @nothinHere THEN
        SET set_id = 1;
      ELSE
        SET set_id = set_id+1;
      END IF;
    END; -- END SET ID BLOCK

    OPEN curl;

    the_loop: LOOP
      FETCH curl
      INTO the_table_name;

      IF done
      THEN
        LEAVE the_loop;
      END IF;

      SET @replaceCheck = CONCAT('INSERT INTO mysmartb_msbdev.blog_manage_sp_results (SELECT NULL, ', set_id,', ', QUOTE('PostMeta Search'),', ',QUOTE(the_table_name),', ',QUOTE(searchStr),', ',QUOTE(replaceStr),', post_id, meta_key, meta_value, NOW(), meta_id, ', QUOTE('Active'),', NULL FROM mysmartb_wrdp1.', the_table_name,
                                ' WHERE meta_value LIKE ', QUOTE(CONCAT('%', searchStr, '%')),');');

      PREPARE stmt FROM @replaceCheck;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;

    END LOOP the_loop;

    CLOSE curl;
END
$$

CALL wpPostMetaSearch('seven', 'seven')

---- Option Search

DELIMITER $$
DROP PROCEDURE IF EXISTS wpOptionSearch $$
CREATE PROCEDURE wpOptionSearch(searchStr TEXT, replaceStr TEXT)
BEGIN
    DECLARE done BOOL;
    DECLARE the_table_name VARCHAR(64);
    DECLARE set_id INT;
    DECLARE curl CURSOR FOR SELECT
                              information_schema.TABLES.TABLE_NAME
                            FROM information_schema.TABLES
                            WHERE
                              information_schema.TABLES.TABLE_SCHEMA = 'mysmartb_wrdp1' AND
                              information_schema.TABLES.TABLE_NAME NOT LIKE 'wp\_wlm\_options' AND
                              (information_schema.TABLES.TABLE_NAME LIKE 'wp\___\_options' OR
                              information_schema.TABLES.TABLE_NAME LIKE 'wp\____\_options');

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    BEGIN -- SET ID BLOCK
      DECLARE CONTINUE HANDLER FOR NOT FOUND SET @nothinHere = TRUE;
      SELECT result_set_id INTO set_id FROM mysmartb_msbdev.blog_manage_sp_results ORDER BY id DESC LIMIT 1;

      IF @nothinHere THEN
        SET set_id = 1;
      ELSE
        SET set_id = set_id+1;
      END IF;
    END; -- END SET ID BLOCK

    OPEN curl;

    the_loop: LOOP
      FETCH curl
      INTO the_table_name;

      IF done
      THEN
        LEAVE the_loop;
      END IF;

      SET @replaceCheck = CONCAT('INSERT INTO mysmartb_msbdev.blog_manage_sp_results (SELECT NULL, ', set_id,', ', QUOTE('Option Search'),', ',QUOTE(the_table_name),', ',QUOTE(searchStr),', ',QUOTE(replaceStr),', option_id, option_name, option_value, NOW(), 0, ', QUOTE('Active'),', NULL FROM mysmartb_wrdp1.', the_table_name,
                                ' WHERE option_value LIKE ', QUOTE(CONCAT('%', searchStr, '%')),');');

      PREPARE stmt FROM @replaceCheck;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;

    END LOOP the_loop;

    CLOSE curl;
END
$$

CALL wpOptionSearch('seven', 'seven')


---- Replace Check

DELIMITER $$
DROP PROCEDURE IF EXISTS blogsReplaceCheck $$
CREATE PROCEDURE blogsReplaceCheck(searchStr TEXT, replaceStr TEXT)
BEGIN
    DECLARE done BOOL;
    DECLARE the_table_name VARCHAR(64);
    DECLARE set_id INT;
    DECLARE curl CURSOR FOR SELECT
                              information_schema.TABLES.TABLE_NAME
                            FROM information_schema.TABLES
                            WHERE
                              information_schema.TABLES.TABLE_SCHEMA = 'mysmartb_wrdp1' AND
                              information_schema.TABLES.TABLE_NAME LIKE '%_posts';

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    BEGIN -- SET ID BLOCK
      DECLARE CONTINUE HANDLER FOR NOT FOUND SET @nothinHere = TRUE;
      SELECT result_set_id INTO set_id FROM mysmartb_msbdev.blog_manage_sp_results ORDER BY id DESC LIMIT 1;

      IF @nothinHere THEN
        SET set_id = 1;
      ELSE
        SET set_id = set_id+1;
      END IF;
    END; -- END SET ID BLOCK

    OPEN curl;

    the_loop: LOOP
      FETCH curl
      INTO the_table_name;

      IF done
      THEN
        LEAVE the_loop;
      END IF;

      SET @replaceCheck = CONCAT('INSERT INTO mysmartb_msbdev.blog_manage_sp_results (SELECT NULL, ', set_id,', ', QUOTE('Replace'),', ',QUOTE(the_table_name),', ',QUOTE(searchStr),', ',QUOTE(replaceStr),', id, post_title, post_name, post_date, guid, ', QUOTE('Active'),', NULL FROM mysmartb_wrdp1.', the_table_name,
                                ' WHERE (post_type = ',QUOTE('post'),' OR post_type = ',QUOTE('page'),') AND post_content LIKE ', QUOTE(CONCAT('%', searchStr, '%')),');');

      PREPARE stmt FROM @replaceCheck;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;

    END LOOP the_loop;

    CLOSE curl;
END
$$

---- Usage Examples

CALL blogsReplaceCheck('seven')

---- Replace Post Strings

DELIMITER $$
DROP PROCEDURE IF EXISTS blogsReplace $$
CREATE PROCEDURE blogsReplace(searchStr TEXT, strReplace TEXT, resultSetId INT)
BEGIN
    DECLARE done BOOL;
    DECLARE the_table_name VARCHAR(64);
    DECLARE the_post_ids BLOB;
    DECLARE curl CURSOR FOR SELECT
                              mysmartb_msbdev.blog_manage_sp_results.table_name, GROUP_CONCAT(mysmartb_msbdev.blog_manage_sp_results.blog_post_id)
                            FROM mysmartb_msbdev.blog_manage_sp_results
                            WHERE
                              mysmartb_msbdev.blog_manage_sp_results.result_set_id = resultSetId AND set_type = 'Replace'
                            GROUP BY
                              mysmartb_msbdev.blog_manage_sp_results.table_name;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN curl;

    the_loop: LOOP
    FETCH curl
    INTO the_table_name, the_post_ids;

      IF done
      THEN
        LEAVE the_loop;
      END IF;

      SET @replaceStr = CONCAT('UPDATE mysmartb_wrdp1.', the_table_name, ' SET post_content=replace(post_content, ', QUOTE(searchStr),
                               ', ', QUOTE(strReplace), ') WHERE ID IN(',the_post_ids,');');

      PREPARE stmt FROM @replaceStr;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;

    END LOOP the_loop;

    CLOSE curl;
  END
$$

---- Usage Examples

CALL blogsReplace('Seller Info Here', 'Seller1 Info Here', 3)

-- ==========================
-- REMOVE STORED PROCEDURES
-- ==========================

---- Remove Check

DELIMITER $$
DROP PROCEDURE IF EXISTS blogsRemoveCheck $$
CREATE PROCEDURE blogsRemoveCheck(post_name TEXT)
BEGIN
    DECLARE done BOOL;
    DECLARE the_table_name VARCHAR(64);
    DECLARE set_id INT;
    DECLARE curl CURSOR FOR SELECT
                              information_schema.TABLES.TABLE_NAME
                            FROM information_schema.TABLES
                            WHERE
                              information_schema.TABLES.TABLE_SCHEMA = 'mysmartb_wrdp1' AND
                              information_schema.TABLES.TABLE_NAME LIKE '%_posts';

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    BEGIN -- SET ID BLOCK
      DECLARE CONTINUE HANDLER FOR NOT FOUND SET @nothinHere = TRUE;
      SELECT result_set_id INTO set_id FROM mysmartb_msbdev.blog_manage_sp_results ORDER BY id DESC LIMIT 1;

      IF @nothinHere THEN
        SET set_id = 1;
      ELSE
        SET set_id = set_id+1;
      END IF;
    END; -- END SET ID BLOCK

    OPEN curl;

    the_loop: LOOP
      FETCH curl
      INTO the_table_name;

      IF done
      THEN
        LEAVE the_loop;
      END IF;

      SET @removeCheck = CONCAT('INSERT INTO mysmartb_msbdev.blog_manage_sp_results (SELECT NULL, ', set_id,', ', QUOTE('Remove'),',',QUOTE(the_table_name),', ',QUOTE(post_name),', ',QUOTE(''),', id, post_title, post_name, post_date, guid, ',QUOTE('Active'),', NULL FROM mysmartb_wrdp1.', the_table_name,
                                ' WHERE post_type = ',QUOTE('post'),' AND post_name = ',QUOTE(post_name),');');

      PREPARE stmt FROM @removeCheck;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;

    END LOOP the_loop;

    CLOSE curl;
END
$$

---- Usage Examples

CALL blogsRemoveCheck('home')

---- Remove Posts

DELIMITER $$
DROP PROCEDURE IF EXISTS blogsRemove $$
CREATE PROCEDURE blogsRemove(resultSetId INT)
BEGIN
    DECLARE done BOOL;
    DECLARE the_table_name VARCHAR(64);
    DECLARE the_post_ids BLOB;
    DECLARE curl CURSOR FOR SELECT
                              mysmartb_msbdev.blog_manage_sp_results.table_name, GROUP_CONCAT(mysmartb_msbdev.blog_manage_sp_results.blog_post_id)
                            FROM mysmartb_msbdev.blog_manage_sp_results
                            WHERE
                              mysmartb_msbdev.blog_manage_sp_results.result_set_id = resultSetId AND set_type = 'Remove'
                            GROUP BY
                              mysmartb_msbdev.blog_manage_sp_results.table_name;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN curl;

    the_loop: LOOP
    FETCH curl
    INTO the_table_name, the_post_ids;

      IF done
      THEN
        LEAVE the_loop;
      END IF;

      SET @removeStr = CONCAT('DELETE FROM mysmartb_wrdp1.', the_table_name, ' WHERE ID IN(',the_post_ids,');');

      PREPARE stmt FROM @removeStr;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;

    END LOOP the_loop;

    CLOSE curl;
  END
$$

---- Usage Examples

CALL blogsRemove(5)

-- ==========================
-- REPLACE TITLE PROCEDURES
-- ==========================

---- Replace Title Check

DELIMITER $$
DROP PROCEDURE IF EXISTS titleReplaceCheck $$
CREATE PROCEDURE titleReplaceCheck(post_name TEXT, titleStr TEXT)
BEGIN
    DECLARE done BOOL;
    DECLARE the_table_name VARCHAR(64);
    DECLARE set_id INT;
    DECLARE curl CURSOR FOR SELECT
                              information_schema.TABLES.TABLE_NAME
                            FROM information_schema.TABLES
                            WHERE
                              information_schema.TABLES.TABLE_SCHEMA = 'mysmartb_wrdp1' AND
                              information_schema.TABLES.TABLE_NAME LIKE '%_posts';

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    BEGIN -- SET ID BLOCK
      DECLARE CONTINUE HANDLER FOR NOT FOUND SET @nothinHere = TRUE;
      SELECT result_set_id INTO set_id FROM mysmartb_msbdev.blog_manage_sp_results ORDER BY id DESC LIMIT 1;

      IF @nothinHere THEN
        SET set_id = 1;
      ELSE
        SET set_id = set_id+1;
      END IF;
    END; -- END SET ID BLOCK

    OPEN curl;

    the_loop: LOOP
      FETCH curl
      INTO the_table_name;

      IF done
      THEN
        LEAVE the_loop;
      END IF;

      SET @removeCheck = CONCAT('INSERT INTO mysmartb_msbdev.blog_manage_sp_results (SELECT NULL, ', set_id,', ', QUOTE('Title Replace'),',',QUOTE(the_table_name),', ',QUOTE(post_name),', ',QUOTE(titleStr),', id, post_title, post_name, post_date, guid, ',QUOTE('Active'),', NULL FROM mysmartb_wrdp1.', the_table_name,
                                ' WHERE post_type = ',QUOTE('post'),' AND post_type = ',QUOTE('page'),' AND post_name = ',QUOTE(post_name),');');

      PREPARE stmt FROM @removeCheck;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;

    END LOOP the_loop;

    CLOSE curl;
END
$$

---- Usage Examples

CALL replaceTitle('home')

---- Replace Title

DELIMITER $$
DROP PROCEDURE IF EXISTS titleReplace $$
CREATE PROCEDURE titleReplace(newTitle VARCHAR(200), newURL VARCHAR(200), resultSetId INT)
BEGIN
    DECLARE done BOOL;
    DECLARE the_table_name VARCHAR(64);
    DECLARE the_post_ids BLOB;
    DECLARE curl CURSOR FOR SELECT
                              mysmartb_msbdev.blog_manage_sp_results.table_name, GROUP_CONCAT(mysmartb_msbdev.blog_manage_sp_results.blog_post_id)
                            FROM mysmartb_msbdev.blog_manage_sp_results
                            WHERE
                              mysmartb_msbdev.blog_manage_sp_results.result_set_id = resultSetId AND set_type = 'Title Replace'
                            GROUP BY
                              mysmartb_msbdev.blog_manage_sp_results.table_name;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN curl;

    the_loop: LOOP
    FETCH curl
    INTO the_table_name, the_post_ids;

      IF done
      THEN
        LEAVE the_loop;
      END IF;

      SET @replaceStr = CONCAT('UPDATE mysmartb_wrdp1.', the_table_name, ' SET post_title=',QUOTE(newTitle),', post_name=',QUOTE(newURL),' WHERE id IN(',the_post_ids,');');

      PREPARE stmt FROM @replaceStr;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;

    END LOOP the_loop;

    CLOSE curl;
  END
$$

---- Usage Examples

CALL titleReplace('HIghland Park Upscale Real Estate Two', 'highland-park-upscale-real-estate-two', 15);

---- Old Fall Backs

SELECT result_set_id INTO set_id FROM mysmartb_msbdev.blog_manage_sp_results ORDER BY id DESC LIMIT 1;
INSERT INTO mysmartb_msbdev.blog_manage_sp_results (SELECT NULL, 1, 'wp_106_posts', id, 'seven', post_title, post_name, post_date, guid, NULL FROM mysmartb_wrdp1.wp_106_posts WHERE post_content LIKE '%seven%');

SELECT
  mysmartb_msbdev.blog_manage_sp_results.table_name, CONVERT(GROUP_CONCAT(mysmartb_msbdev.blog_manage_sp_results.blog_post_id) USING 'utf8')
FROM mysmartb_msbdev.blog_manage_sp_results
WHERE
  mysmartb_msbdev.blog_manage_sp_results.result_set_id = 1
GROUP BY
  mysmartb_msbdev.blog_manage_sp_results.table_name

SELECT * FROM wp_106_posts WHERE ID IN(1686,1446,1471,1562,1570,1576,1591,1593,1619,1620,1642,1648,1670,1671,1438,1409,136,148,1253,149,1249,1250,1252,1254,1407,1381,1380,1356,1350,1255);
