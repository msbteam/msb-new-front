SELECT *
FROM `user_membership`
WHERE
YEAR( account_last_updated ) = YEAR( CURDATE( ) )
AND MONTH(account_last_updated) = MONTH( CURDATE( ) )
AND YEAR(account_created_ts) = YEAR( CURDATE( ) )
AND MONTH(account_created_ts) = MONTH( CURDATE( ) )
AND account_autorenew=1

---

INSERT IGNORE INTO bc_search_index (user_id, index_string)
SELECT
up.user_id main_user_id,
CONCAT_WS(' ',
          uap.blog_uri,
          uap.blog_title,
          upi.user_job_title,
          lu.username,
          up.first_name,
          up.last_name,
          up.email_address,
          up.company_name,
          up.phone_number) AS index_string
FROM user_profile up
LEFT JOIN user_personal_information upi ON up.user_id = upi.user_id
LEFT JOIN login_users lu ON up.user_id = lu.user_id
SELECT user_id, index_string,
MATCH (index_string) AGAINST ('Jim*' IN BOOLEAN MODE) AS search_score
FROM bc_search_index
WHERE MATCH (index_string) AGAINST ('Jim*' IN BOOLEAN MODE)


SELECT DISTINCT
si.user_id 'Customer ID',
up.first_name,
up.last_name,
up.email_address,
up.company_name,
up.phone_number,
ubi.brand_id,
bc.brand_name,
uc.channel_id 'Channel ID',
ch.channel_name 'Channel Name',
um.account_is_active,
si.index_string AS 'URL',
um.account_renewal_date AS 'Timestamp'
FROM bc_search_index si
LEFT JOIN user_profile up ON si.user_id = up.user_id
LEFT JOIN user_branded_id ubi ON ubi.user_id = up.user_id
LEFT JOIN user_channel uc ON ubi.user_id = uc.user_id
LEFT JOIN brands_config bc ON bc.brand_id = ubi.brand_id
LEFT JOIN channels ch ON uc.channel_id = ch.channel_id
RIGHT JOIN user_membership um ON si.user_id = um.user_id
ORDER BY um.account_is_active DESC


SELECT DISTINCT
up.user_id 'Customer ID',
up.first_name,
up.last_name,
up.email_address,
up.company_name,
up.phone_number,
ubi.brand_id,
bc.brand_name,
gl.group_name,
uc.channel_id 'Channel ID',
ch.channel_name 'Channel Name',
um.account_is_active,
um.account_autorenew,
CONCAT_WS(',',uap.blog_uri) 'Websites',
GROUP_CONCAT(uap.blog_uri SEPARATOR ', ') 'Websites'
FROM user_profile up
LEFT JOIN user_branded_id ubi ON ubi.user_id = up.user_id
LEFT JOIN user_channel uc ON ubi.user_id = uc.user_id
LEFT JOIN brands_config bc ON bc.brand_id = ubi.brand_id
LEFT JOIN channels ch ON uc.channel_id = ch.channel_id
LEFT JOIN user_group_id ugi ON ugi.user_id = up.user_id
LEFT JOIN user_autopost uap ON up.user_id = uap.user_id
RIGHT JOIN groups_list gl ON gl.group_id = ugi.group_id
RIGHT JOIN user_membership um ON up.user_id = um.user_id
WHERE uap.blog_uri LIKE '%.waterstoneblog.com%' AND um.account_is_active = 1
ORDER BY um.account_is_active DESC, up.user_id DESC

SELECT DISTINCT
up.user_id 'Customer ID',
up.first_name,
up.last_name,
up.email_address,
up.company_name,
up.phone_number,
ubi.brand_id,
bc.brand_name,
gl.group_name,
uc.channel_id 'Channel ID',
ch.channel_name 'Channel Name',
um.account_is_active,
um.account_autorenew,
CONCAT_WS(',',uap.blog_uri) 'Websites'
FROM user_profile up
LEFT JOIN user_branded_id ubi ON ubi.user_id = up.user_id
LEFT JOIN user_channel uc ON ubi.user_id = uc.user_id
LEFT JOIN brands_config bc ON bc.brand_id = ubi.brand_id
LEFT JOIN channels ch ON uc.channel_id = ch.channel_id
LEFT JOIN user_group_id ugi ON ugi.user_id = up.user_id
LEFT JOIN user_autopost uap ON up.user_id = uap.user_id
RIGHT JOIN groups_list gl ON gl.group_id = ugi.group_id
RIGHT JOIN user_membership um ON up.user_id = um.user_id
WHERE uap.blog_uri LIKE '%.waterstoneblog.com%' AND um.account_is_active = 1
ORDER BY um.account_is_active DESC, up.user_id DESC


SELECT DISTINCT
up.user_id 'Customer ID',
up.first_name,
up.last_name,
up.email_address,
up.company_name,
up.phone_number,
ubi.brand_id,
bc.brand_name,
gl.group_name,
uc.channel_id 'Channel ID',
ch.channel_name 'Channel Name',
um.account_is_active,
um.account_autorenew,
CONCAT_WS(',',uap.blog_uri) 'Websites'
FROM user_profile up
LEFT JOIN user_branded_id ubi ON ubi.user_id = up.user_id
LEFT JOIN user_channel uc ON ubi.user_id = uc.user_id
LEFT JOIN brands_config bc ON bc.brand_id = ubi.brand_id
LEFT JOIN channels ch ON uc.channel_id = ch.channel_id
LEFT JOIN user_group_id ugi ON ugi.user_id = up.user_id
LEFT JOIN user_autopost uap ON up.user_id = uap.user_id
LEFT JOIN groups_list gl ON gl.group_id = ugi.group_id
LEFT JOIN user_membership um ON up.user_id = um.user_id
WHERE ubi.brand_id = 1 AND account_is_active = 1
ORDER BY um.account_is_active DESC, up.user_id DESC


SELECT DISTINCT
ch.channel_id,
ch.channel_name,
sum(CASE WHEN um.account_is_active = 1 THEN 1 ELSE 0 END) active_subs,
sum(CASE WHEN um.account_is_active = 0 THEN 1 ELSE 0 END) inactive
FROM user_profile up
LEFT JOIN user_branded_id ubi ON ubi.user_id = up.user_id
LEFT JOIN user_channel uc ON ubi.user_id = uc.user_id
LEFT JOIN brands_config bc ON bc.brand_id = ubi.brand_id
LEFT JOIN channels ch ON uc.channel_id = ch.channel_id
LEFT JOIN user_group_id ugi ON ugi.user_id = up.user_id
LEFT JOIN groups_list gl ON gl.group_id = ugi.group_id
RIGHT JOIN user_membership um ON up.user_id = um.user_id
GROUP BY ch.channel_id
ORDER BY um.account_is_active DESC, up.user_id DESC


/* ============================== OTHER COMBINATORS ============================== */

INSERT IGNORE INTO mcdb_cust_search_index (cid, information) SELECT c.id customer_id, CONCAT_WS(' ', c.first_name, c.last_name, c.email_address, c.address_01, c.address_02, c.city, c.state, c.zip, c.country, c.phone) FROM mcdb_customers c

INSERT IGNORE INTO mcdb_cust_search_index (scid, information)
SELECT
c.id customer_id,
CONCAT_WS(' ', c.ship_first_name, c.ship_last_name, c.email, c.ship_address1, c.ship_address2, c.ship_city, c.ship_state, c.ship_zip, c.ship_country, c.phone)
FROM nss_users c

SELECT cid, information,
MATCH (information) AGAINST ('Jim Robinson CA' IN NATURAL LANGUAGE MODE) AS search_score
FROM mcdb_cust_search_index
WHERE MATCH (information) AGAINST ('Jim Robinson CA 32767' IN NATURAL LANGUAGE MODE)

INSERT IGNORE INTO mcdb_customers (customer_type, shopping_cart_uid, first_name, last_name, email_address, address_01, address_02, city, state, zip, country, phone, date_added, date_updated, update_by_user, status)
SELECT 'Shopping Cart',
		sc.id,
		sc.bill_first_name,
		sc.bill_last_name,
		sc.email,
		sc.bill_address1,
		sc.bill_address2,
		sc.bill_city,
		sc.bill_state,
		sc.bill_zip,
		mcdbc.iso AS country_id,
		replace(replace(replace(replace(replace(sc.phone, '-', ''), '.', ''), ' ', ''), ')', ''), '(', ''),
		NOW(),
		NOW(),
		1,
		1
		FROM nss_users sc
		LEFT JOIN mcdb_countries AS mcdbc ON sc.bill_country = mcdbc.iso
		WHERE (SELECT count(mcc.id)
			FROM mcdb_customers mcc
			WHERE sc.bill_first_name = mcc.first_name
			AND sc.bill_last_name = mcc.last_name
			AND sc.bill_state = mcc.state
			AND sc.bill_zip = mcc.zip) > 0

/* end select */

SELECT concat('C21P', lpad(id,10,'0')) iv_val, mco.* FROM mcdb_orders mco

UPDATE mcdb_orders SET invoice_number = concat('C21P', id) WHERE invoice_number = '';
UPDATE nss_orders SET invoice_number = concat('C21S', id) WHERE invoice_number = '';

UPDATE mcdb_customers mcc SET country = (SELECT iso FROM mcdb_countries WHERE id = mcc.country) WHERE country REGEXP '[0-9]'

(SELECT 'Phone/Other', first_name, last_name, email, city, state, phone, country, order_total FROM mcdb_orders WHERE customer_id = '15154')
UNION
(SELECT 'Shopping Cart', ship_first_name, ship_last_name, email, ship_city, ship_state, phone, ship_country, total FROM nss_orders WHERE userid = '10612')

UPDATE mcdb_orders mco SET mco.shipping_id = mso.sid
LEFT JOIN mcdb_shipping_addr mso ON
mco.first_name = mso.first_name AND
mco.last_name = mso.last_name AND
mco.address_01 = mso.address_01 AND
mco.address_02 = mso.address_02 AND
mco.city = mso.city AND
mco.state = mso.state AND
mco.zip = mso.zipcode AND
mco.country = mso.zipcode

SELECT mco.* FROM mcdb_orders mco
LEFT JOIN mcdb_shipping_addr mso ON
mco.first_name = mso.first_name AND
mco.last_name = mso.last_name AND
mco.address_01 = mso.address_01 AND
mco.address_02 = mso.address_02 AND
mco.city = mso.city AND
mco.state = mso.state AND
mco.zipcode = mso.zip AND
mco.country = mso.country
LIMIT 1

/* ============================== OTHER COMBINATORS ============================== */

SELECT mcdbo.id AS mcbdo_id, mcdbo.date_ordered AS mcdbo_do, mcdbc.* FROM mcdb_orders mcdbo INNER JOIN mcdb_customers mcdbc ON mcdbo.customer_id = mcdbc.id
WHERE mcdbo.date_ordered >= '2009-1-1'
GROUP BY mcdbo.customer_id

DELETE mccdb from mcdb_customers mccdb INNER JOIN
    (select  min(mcd1.id) AS minid, mcd1.first_name, mcd1.last_name,
             mcd1.city, mcd1.state, mcd1.zip
     FROM mcdb_customers mcd1
     group by mcd1.first_name, mcd1.last_name, mcd1.city,
              mcd1.state, mcd1.zip
     having count(1) > 1) as duplicates
   on (duplicates.first_name = mccdb.first_name
   and duplicates.last_name = mccdb.last_name
   and duplicates.city = mccdb.city
   and duplicates.state = mccdb.state
   and duplicates.zip = mccdb.zip)
UNION ALL
SELECT mcdbo.id, mccd.id FROM mcdb_orders mcdbo, mcdb_customers mccd
WHERE
mcdbo.first_name = mccd.first_name AND
mcdbo.last_name = mccd.last_name AND
mcdbo.city = mccd.city AND
mcdbo.state = mccd.state AND
mcdbo.zipcode = mccd.zip AND
mcdbo.country = mccd.country
GROUP BY mccd.id

/* ------------------------------------------------------------------------ */

SELECT nso.id, nso.order_date, nso.ship_city, nso.ship_state, nso.ship_zip,
(SELECT group_concat(title)
FROM nss_orders_products nsop
INNER JOIN nss_products nsp ON nsop.productid = nsp.id
WHERE nsop.orderid = nso.id) AS items_ordered,
nso.ship_price, nso.tax, nso.subtotal, nso.total
FROM `nss_orders` AS nso
WHERE nso.`order_date` > '2011-10-1' AND nso.`order_date` < '2011-12-31'
AND nso.ship_state LIKE 'CA'
UNION
SELECT '', '', '', nso.ship_state, '', '', sum(nso.ship_price), sum(nso.tax), sum(nso.subtotal), sum(nso.total)
FROM `nss_orders` AS nso
WHERE nso.`order_date` > '2011-10-1' AND nso.`order_date` < '2011-12-31'
AND nso.ship_state LIKE 'CA'
GROUP BY nso.ship_state

/* ------------------------------------------------------------------------ */

SELECT nso.id, nso.date_ordered, nso.city, nso.state, nso.zipcode,
(SELECT group_concat(title)
FROM mcdb_order_details nsop
INNER JOIN nss_products nsp ON nsop.product_id = nsp.id
WHERE nsop.order_id = nso.id) AS items_ordered,
nso.shipping_costs, nso.sales_tax, nso.sales_tax_rate, (nso.order_total - (nso.shipping_costs + nso.sales_tax)) AS subtotals, nso.order_total
FROM `mcdb_orders` AS nso
WHERE MONTH(nso.`date_ordered`) IN (1,2,3) AND YEAR(nso.`date_ordered`) = 2012
AND nso.state LIKE 'CA' AND order_source = 'Phone'
UNION
SELECT '', '', '', nso.state, '', '', sum(nso.shipping_costs), sum(nso.sales_tax), '', sum(nso.order_total - (nso.shipping_costs + nso.sales_tax)) AS subtotal, sum(nso.order_total)
FROM `mcdb_orders` AS nso
WHERE MONTH(nso.`date_ordered`) IN (1,2,3) AND YEAR(nso.`date_ordered`) = 2012
AND nso.state LIKE 'CA' AND order_source = 'Phone'
GROUP BY nso.state

/* ------------------------------------------------------------------------ */

sum( (nso.shipping_costs + (nso.order_total - (nso.shipping_costs + nso.sales_tax))) * (nso.sales_tax_rate / 100) )

/* ------------------------------------------------------------------------ */

INSERT IGNORE INTO mcdb_tax_reports (order_id, order_source, report_year, quarter, county, order_date, zipcode, city, price, shipping, total_collected, tax_collected, tax_rate)
					SELECT nso.id, 'Web', '2012', '1',
					(SELECT county FROM mcdb_zipcode_db mzd WHERE mzd.zipcode = SUBSTRING(trim(nso.ship_zip),1,5) LIMIT 1),
					nso.order_date, trim(nso.ship_zip),
					(SELECT city FROM mcdb_zipcode_db mzd WHERE mzd.zipcode = SUBSTRING(trim(nso.ship_zip),1,5) LIMIT 1),
					nso.subtotal, nso.ship_price, nso.total, nso.tax, round((nso.tax / (nso.subtotal + nso.ship_price)) * 100,2)
					FROM `nss_orders` AS nso
					WHERE MONTH(nso.`order_date`) IN (1,2,3) AND YEAR(nso.`order_date`) = 2012 AND nso1.`status` = 'Shipped/Complete'
					AND nso.ship_state LIKE 'CA'

INSERT IGNORE INTO mcdb_tax_reports (order_id, order_source, report_year, quarter, county, order_date, zipcode, city, price, shipping, total_collected, tax_collected, tax_rate)
					SELECT nso.id, 'Phone/Other', '2012', '1',
					(SELECT county FROM mcdb_zipcode_db mzd WHERE mzd.zipcode = SUBSTRING(trim(nso.zipcode),1,5) LIMIT 1),
					nso.date_ordered, trim(nso.zipcode),
					(SELECT city FROM mcdb_zipcode_db mzd WHERE mzd.zipcode = SUBSTRING(trim(nso.zipcode),1,5) LIMIT 1),
					(nso.order_total - (nso.shipping_costs + nso.sales_tax)) AS subtotal,
					nso.shipping_costs, nso.order_total, nso.sales_tax, round((nso.sales_tax / ((nso.order_total - (nso.shipping_costs + nso.sales_tax)) + nso.shipping_costs)) * 100,2)
					FROM `mcdb_orders` AS nso
					WHERE MONTH(nso.`date_ordered`) IN (1,2,3) AND YEAR(nso.`date_ordered`) = 2012 AND nso2.`order_status` <> 0
					AND nso.`state` LIKE 'CA'

/* ------------------------------------------------------------------------ */

SELECT nso1.id AS order_id, 'Web' AS order_source, '2012' AS report_year, '1' AS quarter,
					(SELECT county FROM mcdb_zipcode_db mzd WHERE mzd.zipcode = SUBSTRING(trim(nso1.ship_zip),1,5) LIMIT 1) AS county,
					nso1.order_date AS order_date, trim(nso1.ship_zip) AS zipcode,
					(SELECT city FROM mcdb_zipcode_db mzd WHERE mzd.zipcode = SUBSTRING(trim(nso1.ship_zip),1,5) LIMIT 1) AS city,
					nso1.subtotal AS price, nso1.ship_price AS shipping, nso1.total AS total_collected, nso1.tax AS tax_collected, round((nso1.tax / (nso1.subtotal + nso1.ship_price)) * 100,2) AS tax_rate
					FROM `nss_orders` AS nso1
					WHERE MONTH(nso1.`order_date`) IN (1,2,3) AND YEAR(nso1.`order_date`) = 2012 AND nso1.`status` = 'Shipped/Complete'
					AND nso1.ship_state LIKE 'CA'
UNION
SELECT nso2.id, 'Phone/Other', '2012', '1',
					(SELECT county FROM mcdb_zipcode_db mzd WHERE mzd.zipcode = SUBSTRING(trim(nso2.zipcode),1,5) LIMIT 1),
					nso2.date_ordered, trim(nso2.zipcode),
					(SELECT city FROM mcdb_zipcode_db mzd WHERE mzd.zipcode = SUBSTRING(trim(nso2.zipcode),1,5) LIMIT 1),
					(nso2.order_total - (nso2.shipping_costs + nso2.sales_tax)) AS subtotal,
					nso2.shipping_costs, nso2.order_total, nso2.sales_tax, round((nso2.sales_tax / ((nso2.order_total - (nso2.shipping_costs + nso2.sales_tax)) + nso2.shipping_costs)) * 100,2)
					FROM `mcdb_orders` AS nso2
					WHERE MONTH(nso2.`date_ordered`) IN (1,2,3) AND YEAR(nso2.`date_ordered`) = 2012 AND nso2.`order_status` <> 0
					AND nso2.`state` LIKE 'CA'