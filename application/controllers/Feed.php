
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Feed extends User_Controller {
	public function add( $uid, $service_id, $auth_id ) {
		$this->load->helper( 'url' );

		if (!empty($uid)) {
			$this->db->query( "INSERT INTO btb_content_posters
			VALUES (
				'',
				" . $this->db->escape($uid) . ",
				" . $this->db->escape($service_id) . ",
				'',
				" . $this->db->escape($auth_id) . ",
				NOW()
			);" );
		}

		redirect( "/control" );
	}

	public function set_facebook_pages() {
		$this->load->helper('form', 'url');
		$this->load->library('form_validation');

		$data = $this->db->query("SELECT * FROM btb_user_auth_storage WHERE user_id = " . $this->db->escape($_SESSION['userid']) . " AND auth_service = 5 LIMIT 1")->row_array();
		$json = json_decode($data['auth_info']);

		if (!empty($_POST['pages']['personal'])) {
			$info[] = (object) array("page_id" => $data['auth_key'], "auth_token" => $data['auth_token']);
		}

		foreach($json->pages as $pages) {
			if (in_array($pages->id, $_POST['pages']))
				$info[] = (object) array("page_id" => $pages->id, "auth_token" => $pages->access_token);
		}

		$this->db->query("DELETE FROM btb_content_posters WHERE user_id = " . $this->db->escape($_SESSION['userid']) . " AND service_id = 5 LIMIT 1");

		$this->db->query("INSERT INTO btb_content_posters SET
												service_info = " . $this->db->escape(json_encode($info)) . ",
												user_id = " . $this->db->escape($_SESSION['userid']) . ",
												auth_storage_id = " . $this->db->escape($data['auth_id']) . ",
												service_id = 5;");

		redirect( "/control" );
	}

	public function read_feed() {
		$this->load->helper('form', 'url');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('feed_url', 'Feed URL', 'required');

		if ($this->form_validation->run() === TRUE) {

			$this->db->query("INSERT INTO btb_content_monitors VALUES
								(
								'',
								" . $this->db->escape($_SESSION['userid']) . ",
								0,
								" . $this->db->escape($this->input->post('feed_title')) . ",
								" . $this->db->escape($this->input->post('feed_url')) . ",
								'on',
								NOW()
								);");

			redirect( "/control" );
		} else {
			$data['error_flush'] = validation_errors('<div class="alert alert-danger" style="margin: 0;">', '</div>');
			$this->load->template('control', $data);
		}

	}

	public function read_feed_remove( $id ) {
		$this->load->helper( 'url' );

		$this->db->query( "DELETE FROM btb_content_monitors WHERE id = " . $this->db->escape($id));
		redirect( "/control" );
	}

	public function remove( $poster_id ) {
		$this->load->helper( 'url' );

		$this->db->query( "DELETE FROM btb_content_posters WHERE poster_id = " . $this->db->escape($poster_id));
		redirect( "/control" );
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */