
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct() {
		// Call the Model constructor
		parent::__construct();

		$this->load->helper( 'url' );
	}

	public function index()
	{
		$this->load->helper('form');
        $this->load->template('login');
    }

	public function submit() {
		$this->load->helper('form');
		$this->load->library('form_validation', 'encrypt');

		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');

		if ($this->form_validation->run() === TRUE) {
			// read user's credentials from db, through Login Model
			// authenticate

			$uname = $this->input->post('username');
			$pwd = md5($this->input->post('password'));

			$this->db->select('id, username, password');
			$query = "SELECT user_id,username FROM btb_login_users WHERE username = " . $this->db->escape($uname) . " AND password = " . $this->db->escape($pwd) . " LIMIT 1";

			$result = $this->db->query($query);

			var_dump($result);

			if ( $result->result_id->num_rows > 0 ) {
				$udata = $result->row(0);
				$this->session->set_userdata('login_state', TRUE);
				$this->session->set_userdata('username', $udata->username);
				$this->session->set_userdata('userid', $udata->user_id);
				redirect("/control");
			} else {
				//$error['invalid'] = "Invalid Username/Password";
				//$this->load->template('login', $error);
			}
		} else {
			//$this->load->template('login');
			//echo "form validation false";
		}
	}

	public function logout() {
		$this->session->sess_destroy();
		redirect("/");
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */