<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once('../libraries/phpmailer/PHPMailerAutoload.php');

class mailer extends CI_Model {

    private $headers = array();
    private $mail;

    function __construct()
    {
        parent::__construct();
    }

    private function build_headers() {

        // Temporary
        foreach($this->headers as $header) {

        }
    }

    private function fill_template($template_location, $keywords) {

        $template = file_get_contents($template_location);

        // Assign keywords values to a replacement pattern
        while ( list($key,$value) = each($keywords) )
        {
            $patterns[] = '/##' . $key . '##/';
        }

        // Replace all instances of the keyword array values inside the HTML file
        $content = preg_replace($patterns, $keywords, $template);

        return $content;
    }

    public function send_email($mail_to, $mail_from, $subject, $key_content, $template='default') {
        $this->mail = new PHPMailer();

        $this->mail->IsSendmail(); // telling the class to use SendMail transport

        $this->mail->From       = $mail_from['email'];
        $this->mail->FromName   = $mail_from['name'];

        foreach ($mail_to as $address) {
            $this->mail->AddAddress($address['email'], $address['name']);
        }

        $body = $this->fill_template($_SERVER['DOCUMENT_ROOT'] . '/lib/email/templates/' . $template . '.html', $key_content);
        $this->mail->isHTML(true);
        $this->mail->Subject = $subject;
        $this->mail->MsgHTML($body);

        if(!$this->mail->Send()) {
            $result = "Mailer Error: " . $this->mail->ErrorInfo;
        } else {
            $result = "Message sent!";
        }

        return $result;
    }

}