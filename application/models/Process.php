<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MSB_Process extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    private function get_batch_users($batch_id) {
        $user = array();
        $ap_user_query = mysql_query("SELECT DISTINCT user_id FROM user_blogpost_tracking WHERE batch_id = '" . mysql_real_escape_string($batch_id) . "'");

        while ($data = mysql_fetch_array($ap_user_query)) {
            $user[] = $data['user_id'];
        }

        return $user;
    }

    private function get_post_name($blogpost_id) {
        $data = mysql_fetch_array(mysql_query("SELECT post_title FROM blog_posts WHERE post_id = '" . mysql_real_escape_string($blogpost_id) . "' LIMIT 1"));

        return $data['post_title'];
    }

    private function track_email($email, $name, $user_id, $batch_id, $tracking_array, $response) {

        $tracking_array = json_encode($tracking_array);

        mysql_query("INSERT INTO user_blogpost_email_tracking VALUES
                    ('',
                    '" . mysql_real_escape_string($user_id) . "',
                    '" . mysql_real_escape_string($batch_id) . "',
                    '" . mysql_real_escape_string($tracking_array) . "',
                    '" . mysql_real_escape_string($response) . "',
                    '" . mysql_real_escape_string($email) . "',
                    '" . mysql_real_escape_string($name) . "',
                    NOW()
                    );");
    }

    public function acp_test_user($batch_id, $user_id) {
        include_once($_SERVER['DOCUMENT_ROOT'] . '/lib/email/send_email.php');

        $user = $user_id;
        $mailer = new mailer();
        $content = array();
        $track_post_ids = array();

            unset($content, $to, $track_post_ids);

            $brand_id = getMemberBrandId($user);
            $ap_batch_query = mysql_query("SELECT ua.service_postmode ua_service_mode, ubt.* FROM user_blogpost_tracking ubt
                                            RIGHT JOIN user_autopost ua ON ubt.service_post_id = ua.row_id
                                            WHERE ubt.batch_id = '" . mysql_real_escape_string($batch_id) . "'
                                            AND ubt.user_id = '" . mysql_real_escape_string($user) . "'
                                            ORDER BY ubt.status_result DESC");


            while ($apdata = mysql_fetch_array($ap_batch_query)) {
                $post_name = $this->get_post_name($apdata['post_id']);
                $track_post_ids[] = $apdata['row_id'];
                $post_mode = ($apdata['ua_service_mode'] == 1 ? 'Publish':'Draft');

                if ($apdata['status_result'] == "Success") {

                    $content['content_rows'] .= '<tr>
                                                    <td><h4 style="margin: 0px;">' . $post_name . '</h4>' . $apdata['service_url'] . '</td>
                                                    <td width="200" align="center">' . $apdata['status_message'] . '<br>Publish Status: <strong>' . $post_mode  . '</strong></td>
                                                    <td align="center">&nbsp;</td>
                                                 </tr>';

                } else {
                    $retry_code=generatePassword("16", "4"); // Failed. Make Retry Code.
                    $retry_url = "http://".getBrandRootURL($brand_id)."/republish.php?r=".$retry_code; // Make retry URL
                    addToRetryLog($user, $apdata['row_id'], $apdata['post_id'], $retry_code); // Add to log

                    $content['content_rows'] .= '<tr style="background: #FFEAEB;">
                                                    <td><h4 style="margin: 0px;">' . $post_name . '</h4>' . $apdata['service_url'] . '</td>
                                                    <td width="200" align="center">
                                                        <strong>Status:</strong> ' . $apdata['status_result'] . '<br />
                                                        <strong>HTTP Code:</strong> ' . $apdata['status_code'] . '<br />
                                                        <strong>Reason:</strong> ' . $apdata['status_message'] . '
                                                    </td>
                                                    <td align="center"><a href="' . $retry_url . '" target="_new">REPOST NOW</a></td>
                                                 </tr>';
                }

            }

            $profile = getProfileInfo($user);

            $to[] = array('name' => $profile['first_name']." ".$profile['last_name' ], 'email' => $profile['email_address']);

            $from['name'] = getBrandName($brand_id);
            $from['email'] = getBrandSupportEmail($brand_id);

            $content['footer'] = getBrandEmailSignature($brand_id);
            $content['disclaimer'] = getEmailFooter(1, $user, $brand_id);

            $response = $mailer->send_email($to, $from, 'MySMARTBlog Content Delivery Report', $content, 'autopost_daily');
            $this->track_email($profile['email_address'], $profile['first_name']." ".$profile['last_name' ], $user, $batch_id, $track_post_ids, $response);
    }

    public function autopost_complete_process($batch_id) {
        include_once($_SERVER['DOCUMENT_ROOT'] . '/lib/email/send_email.php');

        $users = $this->get_batch_users($batch_id);
        $mailer = new mailer();
        $content = array();
        $track_post_ids = array();

        foreach($users as $user) {

            unset($content, $to, $track_post_ids);

            if ( isDailyNotifyOn($user) ) {

                $brand_id = getMemberBrandId($user);
                $ap_batch_query = mysql_query("SELECT ua.service_postmode ua_service_mode, ubt.* FROM user_blogpost_tracking ubt
                                                RIGHT JOIN user_autopost ua ON ubt.service_post_id = ua.row_id
                                                WHERE ubt.batch_id = '" . mysql_real_escape_string($batch_id) . "'
                                                AND ubt.user_id = '" . mysql_real_escape_string($user) . "'
                                                ORDER BY ubt.status_result DESC");

                while ($apdata = mysql_fetch_array($ap_batch_query)) {
                    $post_name = $this->get_post_name($apdata['post_id']);
                    $track_post_ids[] = $apdata['row_id'];
                    $post_mode = ($apdata['ua_service_mode'] == 1 ? 'Publish':'Draft');

                    if ($apdata['status_result'] == "Success") {

                        $content['content_rows'] .= '<tr>
                                                        <td><h4 style="margin: 0px;">' . $post_name . '</h4>' . $apdata['service_url'] . '</td>
                                                        <td width="200" align="center">' . $apdata['status_message'] . '<br>Publish Status: <strong>' . $post_mode  . '</strong></td>
                                                        <td align="center">&nbsp;</td>
                                                     </tr>';

                    } else {
                        $retry_code=generatePassword("16", "4"); // Failed. Make Retry Code.
                        $retry_url = "http://".getBrandRootURL($brand_id)."/republish.php?r=".$retry_code; // Make retry URL
                        addToRetryLog($user, $apdata['row_id'], $apdata['post_id'], $retry_code); // Add to log

                        $content['content_rows'] .= '<tr style="background: #FFEAEB;">
                                                        <td><h4 style="margin: 0px;">' . $post_name . '</h4>' . $apdata['service_url'] . '</td>
                                                        <td width="200" align="center">
                                                            <strong>Status:</strong> ' . $apdata['status_result'] . '<br />
                                                            <strong>HTTP Code:</strong> ' . $apdata['status_code'] . '<br />
                                                            <strong>Reason:</strong> ' . $apdata['status_message'] . '
                                                        </td>
                                                        <td align="center"><a href="' . $retry_url . '" target="_new">REPOST NOW</a></td>
                                                     </tr>';
                    }

                }

                $profile = getProfileInfo($user);

                $to[] = array('name' => $profile['first_name']." ".$profile['last_name' ], 'email' => $profile['email_address']);

                $from['name'] = getBrandName($brand_id);
                $from['email'] = "support@smartblogcontent.com";

                $content['footer'] = getBrandEmailSignature($brand_id);
                $content['disclaimer'] = getEmailFooter(1, $user, $brand_id);

                $response = $mailer->send_email($to, $from, 'MySMARTBlog Content Delivery Report', $content, 'autopost_daily');
                $this->track_email($profile['email_address'], $profile['first_name']." ".$profile['last_name' ], $user, $batch_id, $track_post_ids, $response);
            } else {
                $profile = getProfileInfo($user);

                $this->track_email($profile['email_address'], $profile['first_name']." ".$profile['last_name' ], $user, $batch_id, $track_post_ids, 'Skipped: User opt-out.');
            }

            usleep(10000);
        }
    }
}