<div class="main-container">

<!-- #intro -->
<div class="container-fluid top-container">
    <div class="container">
        <div class="col-md-10 col-md-offset-1 text-left">
            <h1 class="georgia text-left">Administration Section</h1>
        </div>
    </div>
</div>
<!-- ends Intro -->

<!-- #contact -->
<div class="container-fluid white">
    <div class="container">
	    <h1>Configuration</h1>
    </div>

	<?php
		$configQuery = "SELECT * FROM btb_content_config";
		$config_data = $this->db->query($configQuery)->row_array();
	?>

    <div class="container">
	    <div class="col-md-6" style="margin-top: 15px;">
		    <a href="http://push.bringtheblog.com:3081/monitor/scan" class="btn-lg btn-info">Scan RSS Feeds</a>
		    <a href="http://push.bringtheblog.com:3082/publish/run" class="btn-lg btn-info">Publish to Services</a>
	    </div>
    </div>

	<div class="container" style="border-bottom: 3px solid #ccc; border-top: 3px solid #ccc;">
		<form action="http://push.bringtheblog.com:3081/monitor/set" method="post">
			<div class="col-md-6">
				<h3>RSS Feed Check Interval</h3>
				<input name="check_interval_value" class="input-lg pull-left" type="text" value="<?=$config_data['check_time_interval']?>" placeholder="E.G. 20">
				<select name="check_interval_unit" class="input-lg pull-left">
					<option<?=($config_data['check_time_unit'] == 'seconds' ? ' selected="selected"' : '')?>>seconds</option>
					<option<?=($config_data['check_time_unit'] == 'minutes' ? ' selected="selected"' : '')?>>minutes</option>
					<option<?=($config_data['check_time_unit'] == 'hours' ? ' selected="selected"' : '')?>>hours</option>
					<option<?=($config_data['check_time_unit'] == 'days' ? ' selected="selected"' : '')?>>days</option>
				</select>
			</div>
			<div class="col-md-6">
				<h3>RSS Feed Capture Window</h3>
				<input name="capture_window_value" class="input-lg pull-left" type="text" value="<?=$config_data['capture_window_value']?>" placeholder="E.G. 30">
				<select name="capture_window_unit" class="input-lg pull-left">
					<option<?=($config_data['capture_window_unit'] == 'seconds' ? ' selected="selected"' : '')?>>seconds</option>
					<option<?=($config_data['capture_window_unit'] == 'minutes' ? ' selected="selected"' : '')?>>minutes</option>
					<option<?=($config_data['capture_window_unit'] == 'hours' ? ' selected="selected"' : '')?>>hours</option>
					<option<?=($config_data['capture_window_unit'] == 'days' ? ' selected="selected"' : '')?>>days</option>
				</select>
			</div>
			<div class="col-md-6" style="margin-top: 15px;">
				<button type="submit" class="btn btn-lg btn-purple">Save Settings</button>
			</div>
		</form>
	</div>

	<div class="container">
		<div class="col-md-4">
			<?php echo $error_flush; ?>

			<h3>Add New Feeds</h3>
			<div class="input-fields">
				<form action="/feed/reader/add" method="post">
					<input name="feed_title" class="input-lg" type="text" placeholder="Feed Title">
					<input name="feed_url" class="input-lg" type="text" placeholder="Feed : http://example.blog.com/feed/">
					<button type="submit" class="btn btn-lg btn-purple">Add New Feed <i class="glyphicon glyphicon-plus"></i></button>
				</form>
			</div>
			<h3>Active Feeds Monitored</h3>
			<div class="list-group">
				<?
				$oauth = "SELECT * FROM btb_content_monitors
								WHERE user_id = " . $this->db->escape($_SESSION['userid']);
				$oauthd = $this->db->query($oauth)->result_array();

				foreach($oauthd as $row) {
						$url = "/feed/reader/remove/" . $row['id'];
				?>
					<a href="<?=$url?>" class="list-group-item">
						<p class="list-group-item-text" style="word-break: break-all;"><strong><?=$row['content_title']?></strong></p>
						<p class="list-group-item-text" style="word-break: break-all;"><?=$row['content_feed_url']?></p>
						<br>
						<p class="list-group-item-text"><strong style="color: blue;">Remove from Monitoring</strong></p>
					</a>
				<? } ?>
			</div>
		</div>
		<div class="col-md-4">
			<h3>Authenticate</h3>
			<p><a href="http://push.bringtheblog.com/auth/facebook/r/<?=$_SESSION['userid']?>" class="btn btn-primary"><i class="glyphicon glyphicon-import"></i> Authenticate with Facebook</a></p>
			<p><a href="http://push.bringtheblog.com/auth/twitter/r/<?=$_SESSION['userid']?>" class="btn btn-info"><i class="glyphicon glyphicon-import"></i> Authenticate with Twitter</a></p>
			<p><a href="http://push.bringtheblog.com/auth/tumblr/r/<?=$_SESSION['userid']?>" class="btn btn-default"><i class="glyphicon glyphicon-import"></i> Authenticate with Tumblr</a></p>
			<p><a href="http://push.bringtheblog.com/auth/blogger/r/<?=$_SESSION['userid']?>" class="btn btn-danger"><i class="glyphicon glyphicon-import"></i> Authenticate with Google</a></p>
            <p><a href="http://push.bringtheblog.com/auth/linkedin/r/<?=$_SESSION['userid']?>" class="btn btn-default" style="background: #000; color: #fff;"><i class="glyphicon glyphicon-import"></i> Authenticate with LinkedIn</a></p>

            <h3>Currently Authenticated</h3>
			<div class="list-group">
				<?
				$oauth = "SELECT buas.*, bs.service_name FROM btb_user_auth_storage buas
								INNER JOIN btb_services bs ON buas.auth_service = bs.service_id
								WHERE user_id = " . $this->db->escape($_SESSION['userid']);
				$oauthd = $this->db->query($oauth)->result_array();

				foreach($oauthd as $row) {

					if ($row['service_name'] == "Twitter")
						$url = "/feed/add/" . $row['user_id'] . "/" . $row['auth_service'] . "/" . $row['auth_id'];
					else if ($row['service_name'] == "Tumblr")
						$url = "/feed/add/" . $row['user_id'] . "/" . $row['auth_service'] . "/" . $row['auth_id'];
					else if ($row['service_name'] == "Blogger")
						$url = "/feed/add/" . $row['user_id'] . "/" . $row['auth_service'] . "/" . $row['auth_id'];
					else
						$url = "#";

				?>
					<a href="<?=$url?>" class="list-group-item">
						<p class="list-group-item-text" style="word-break: break-all;">Service: <?=$row['service_name']?></p>
						<br>
						<? if ($row['service_name'] == "Twitter" || $row['service_name'] == "Tumblr" || $row['service_name'] == "Blogger") { ?>
						<p class="list-group-item-text"><strong style="color: blue;">Click here to add to publishing</strong></p>
						<? } ?>
					</a>
				<? } ?>
			</div>
        </div>
		<div class="col-md-4">
			<h3>Posting to Services</h3>
			<div class="list-group">
			<?
			$oauth = "SELECT bcp.*, bs.service_name FROM btb_content_posters bcp
								INNER JOIN btb_services bs ON bcp.service_id = bs.service_id
								WHERE user_id = " . $this->db->escape($_SESSION['userid']);
			$oauthd = $this->db->query($oauth)->result_array();

			foreach($oauthd as $row) {

				if ($row['service_name'] == "Twitter")
					$url = "/feed/remove/" . $row['poster_id'];
				else if ($row['service_name'] == "Tumblr")
					$url = "/feed/remove/" . $row['poster_id'];
				else if ($row['service_name'] == "Blogger")
					$url = "/feed/remove/" . $row['poster_id'];
				else if ($row['service_name'] == "Facebook")
					$url = "/feed/remove/" . $row['poster_id'];
				else
					$url = "#";

				?>
				<a href="<?=$url?>" class="list-group-item">
					<p class="list-group-item-text" style="word-break: break-all;">Posting to Service: <?=$row['service_name']?></p>
					<br>
					<? if ($row['service_name'] == "Twitter" || $row['service_name'] == "Tumblr" || $row['service_name'] == "Blogger" || $row['service_name'] == "Facebook") { ?>
						<p class="list-group-item-text"><strong style="color: blue;">Remove from Publishing</strong></p>
					<? } ?>
				</a>
			<? } ?>
			</div>
			<?
				$data = $this->db->query("SELECT * FROM btb_user_auth_storage WHERE user_id = " . $this->db->escape($_SESSION['userid']) . " AND auth_service = 5 LIMIT 1")->row_array();
				$content = $this->db->query("SELECT service_info FROM btb_content_posters WHERE user_id = " . $this->db->escape($_SESSION['userid']) . " AND service_id = 5 LIMIT 1")->row_array();
				$JD = json_decode($data['auth_info']);
				$CD = json_decode($content['service_info']);
				$personal_checked = false;

			if (!empty($data)) {
			?>
            <h3>Manage Facebook Pages</h3>
            <form action="/service/set/pages" method="post">
				<?
		            foreach($CD as $page) {
			            if ($data['auth_key'] == $page->page_id) {
				            $personal_checked = true;
			            }
		            }

	            ?>
	            <input type="checkbox" name="pages[personal]" value="<?=$data['auth_key']?>"<?=($personal_checked ? ' checked' : '')?>> Publish to Personal Page<br>
				<?
	                foreach ($JD->pages as $page) {

		                reset($CD);
		                foreach($CD as $check) {
			                if ($page->id == $check->page_id) {
				                $vCheck = true;
				                break;
			                } else {
				                $vCheck = false;
			                }
		                }
	            ?>
                <input type="checkbox" name="pages[<?=$page->id?>]" value="<?=$page->id?>"<?=($vCheck ? ' checked' : '')?>> <?=$page->name?><br>
	            <?
	                }
	            ?>
	            <br>
	            <button type="submit" class="btn btn-lg btn-purple">Save Publish Settings</button>
            </form>
			<? } ?>
		</div>
	</div>

    <div class="container" style="border-top: 3px solid #ccc;">
	    <h1>Publish Queue</h1>

	    <table class="table table-condensed">
		    <thead>
		    <tr>
			    <th>#</th>
			    <th>Post Title</th>
			    <th>Post GUID</th>
		    </tr>
		    </thead>
		    <tbody>
		    <?
		    $publogq = "SELECT bcs.* FROM btb_content_store bcs WHERE bcs.is_published = 0 AND bcs.user_id =" . $this->db->escape($_SESSION['userid']);

		    $publogd = $this->db->query($publogq)->result_array();

		    foreach($publogd as $row) {

			    ?>
			    <tr>
				    <th scope="row"><?=$row['content_id']?></th>
				    <td><?=$row['content_title']?></td>
				    <td><?=$row['blog_post_guid']?></td>
			    </tr>
		    <? } ?>
		    </tbody>
	    </table>

    </div>

	<div class="container" style="border-top: 3px solid #ccc;">
		<h1>Publish Log</h1>

		<table class="table table-condensed">
			<thead>
				<tr>
					<th>#</th>
					<th>Status</th>
					<th>Service</th>
					<th>Post Title</th>
					<th>Info</th>
				</tr>
			</thead>
			<tbody>
			<?
			$publogq = "SELECT bcpl.*, bs.service_name, bcs.user_id, bcs.content_title, bcs.blog_post_guid FROM btb_content_post_logs bcpl
						INNER JOIN btb_services bs ON bcpl.service_id = bs.service_id
						INNER JOIN btb_content_store bcs ON bcpl.content_id = bcs.content_id
						WHERE bcs.user_id = " . $this->db->escape($_SESSION['userid']);


			$publogd = $this->db->query($publogq)->result_array();

			foreach($publogd as $row) {

			?>
				<tr>
					<th scope="row"><?=$row['content_id']?></th>
					<td><?=$row['response_status']?></td>
					<td><?=$row['service_name']?></td>
					<td><?=$row['content_title']?></td>
					<td style="word-break: break-all;"><?=$row['response_info']?></td>
				</tr>
			<? } ?>
			</tbody>
		</table>

	</div>
<!-- End #contact -->
