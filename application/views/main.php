<div class="main-container">

<!-- #intro -->
<div class="container-fluid" id="intro">
    <div class="container">
        <div class="col-md-6">
            <img src="/assets/images/laptop.png" alt=""/>
        </div>
        <div class="col-md-6 text-left">
            <h1 class="georgia text-left">Bring the Blog</h1>

            <p>Welcome to Bring the Blog by MySMARTblog! Blogs are the hub of an effective social marketing strategy and we make the job easy. Every business day we post new, original content to your WordPress and/or Blogger blog with eye-catching graphics and trust-building copy. Blogging has never been simpler.</p>
            <a href="/sign-up" class="btn btn-lg btn-purple"">Get Started Now</a>
        </div>
    </div>
</div>
<!-- ends Intro -->
<!-- #about -->
<div class="container-fluid light" id="about">
    <ul class="container">
        <li class="col-md-6">
            <h3><i class="glyphicon glyphicon-plus-sign" style="float: left; padding-top: 1px; margin-right: 8px;"></i> Original Content Only</h3>
            <p>Bring the Blog content is 100% home-grown and original. Our bloggers are protected from copyright infringement and from ethical blogging violations.</p>
            <h3><i class="glyphicon glyphicon-plus-sign" style="float: left; padding-top: 1px; margin-right: 8px;"></i> New Content Daily</h3>
            <p>If you're not providing fresh and relevant content daily, your clients will get it from someone else. A blog that's updated daily helps build a fence around your database.</p>
        </li>
        <li class="col-md-6">
            <h3><i class="glyphicon glyphicon-plus-sign" style="float: left; padding-top: 1px; margin-right: 8px;"></i> Autopost To WordPress</h3>
            <p>Bring the Blog posts direct to your WordPress blogs, providing content, categories, tags, and a title. Take your posts in "Draft" mode, or have them publish live. Requires Wordpress version 2.9 or up.</p>
            <h3><i class="glyphicon glyphicon-plus-sign" style="float: left; padding-top: 1px; margin-right: 8px;"></i> Need Help?</h3>
            <p>Don't have a WordPress blog? <a href="http://mysmartblog.com/packages-pricing/" style="color: #fff; text-decoration: underline;">Click here</a> and take a look at our packages if you'd like us to set one up for you, or email us here.</p>
        </li>
    </ul>
</div>
<!-- ends About -->

<!-- #what_we_do -->
<div class="container-fluid white" id="what_we_do">
    <div class="container light">
        <h1 class="noshadow">What We Do</h1>

        <div class="row">
            <div class="col-md-3">
                <div class="thumbnail">
                    <img data-src="holder.js/300x300" src="/assets/images/screenshot2.png" alt="...">
                    <div class="caption">
                        <h3>Overview</h3>
                        <p class="text-justify">Blogs are no longer a marketing elective. As the hub of your social marketing strategy, they're a business requirement. Your clients demand top-notch industry content and that's what Bring the Blog delivers direct to your blog every business day. No recipes, no fluff -- our industry writers deliver powerful content that resonates with readers and drives them to action.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="thumbnail">
                    <img data-src="holder.js/300x300" src="/assets/images/screenshot3.png" alt="...">
                    <div class="caption">
                        <h3>Value Proposition</h3>
                        <p class="text-justify">You're a busy person and blogging takes time. A lot of time, actually, if you want to do it right. Bring the Blog reduces the blogging time burden by writing meaningful, easy-to-ready content and publishing it directly to your WordPress and/or Blogger blog every business day. With Bring the Blog, you'll spend less time at your keyboard and more time with your clients.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="thumbnail">
                    <img data-src="holder.js/300x300" src="/assets/images/screenshot5.png" alt="...">
                    <div class="caption">
                        <h3>Pricing</h3>
                        <p class="text-justify">For $97 per month, you can quit your job as the lead journalist of your blog and be its Editor-in-Chief instead. Take your daily content from Bring the Blog, tweak it so it's "yours", then go about your day. There's no contract, you can cancel anytime, and there's a 30-day money back guarantee.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="thumbnail">
                    <img data-src="holder.js/300x300" src="/assets/images/screenshot6.png" alt="...">
                    <div class="caption">
                        <h3>Features</h3>
                        <ul class="text-left" style="list-style: circle;">
                            <li>New, relevant blog content daily</li>
                            <li>Easy-to-read charts to make your blog &quot;pop&quot;</li>
                            <li>Written by industry veterans with education and experience</li>
                            <li>Posts direct to WordPress and Blogger in either &quot;Live&quot; or &quot;Draft&quot; Mode</li>
                            <li>Ready-to-forward marketing material for your database</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ends #what_we_do -->

<!-- #team -->
<div class="container-fluid" id="team">
    <div class="container dark">
        <h1>How We Can Help</h1>

        <div class="row">
            <ul class="thumbnails">
                <li class="col-md-4 col-md-offset-1">
                    <div class="thumbnail" style="background: none;">
                        <img alt="" src="/assets/images/icon2.png">
                    </div>
                </li>
                <li class="col-md-7">
                    <h3>Spend More Time Selling</h3>
                    <p>Writing good blog content takes time. A lot of it. So stop worrying about writing good content for your blog and let us do it for you. Bring the Blog is your in-the-field journalist. You're the expert Editor-In-Chief. You'll spend 5 minutes on your blog each day instead of 60.</p>
                </li>
            </ul>
        </div>
        <div class="row">
            <ul class="thumbnails">
                <li class="col-md-4 col-md-offset-1">
                    <div class="thumbnail" style="background: none;">
                        <img alt="" src="/assets/images/icon1.png">
                    </div>
                </li>
                <li class="col-md-7">
                    <h3>No Long-Term Contracts</h3>
                    <p>When you join Bring the Blog, you won't pay a stiff setup fee or sign a long-term contract. It's because we're pretty convinced that you'll love us once you get to know us. It's also why we offer a 30-day money-back guarantee. Cancel anytime and you'll owe us nothing. No questions asked.</p>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- End Team -->

<!-- #contact -->
<div class="container-fluid" id="contact">
    <div class="container dark">
        <h1>Get into Contact with Us</h1>

        <div class="col-md-5 col-md-offset-1">
            <form action="/contact/submit" method="post">
                <div class="input-fields">
                    <input name="inputName" type="text" id="inputName" placeholder="Name">
                    <input name="inputEmail" type="text" id="inputEmail" placeholder="Email">
                    <textarea id="inputContact" name="inputContact"></textarea>
                    <button type="submit" class="btn btn-purple">Contact Us</button>
                </div>
            </form>
        </div>
        <div class="col-md-4">
            <h2>Get Support</h2>

            <p>A website can only tell you so much. Sometimes you need to ask questions; to hear it straight from the Support Team's mouth. So here you go. Here's your best ways to reach us. Of all the methods, we recommend email because (1) you'll have a lasting record of the conversation and, (2) we tend to work while you sleep. Email is well-suited for that. Either way, let us know how we can help. That's why we're here.</p>
            <address>
                Bring the Blog<br>
                525 N Central<br>
                Avondale, AZ 85323<br>
                United States
            </address>
            <address>
                <abbr title="Phone">P:</abbr> (888) 755-8783
            </address>
            <address>
                <strong>Commonly Asked Questions</strong><br>
                <a href="mailto:support@bringtheblog.com" class="contactlink">support@bringtheblog.com</a>
            </address>
        </div>
    </div>
</div>
<!-- End #contact -->

</div>