<div class="main-container">

<!-- #intro -->
<div class="container-fluid top-container">
    <div class="container">
        <div class="col-md-10 col-md-offset-1 text-left">
            <h1 class="georgia text-left">Sign up for Bring the Blog</h1>
        </div>
    </div>
</div>
<!-- ends Intro -->

<!-- #contact -->
<div class="container-fluid" id="contact">
    <div class="container dark">
        <div class="col-md-10 col-md-offset-1">
            <h1 class="text-left">Let's Get Started!</h1>
            <p>This short form is all that stands between you and your Bring the Blog membership. We've done our best to document some of the most common questions, but if there's something on your mind we haven't covered for you, just send us an email about it.</p>
        </div>

        <div class="col-md-5 col-md-offset-1">
            <form action="/sign-up/submit" method="post">
                <h3>Choose a Content Channel</h3>
                <div class="input-fields">
                    <select name="content_channel">
                        <option value="1">Real Estate</option>
                        <option value="1">Mortage</option>
                    </select>
                </div>
                <h3>A Little About You</h3>
                <div class="input-fields">
                    <input name="first_name" type="text" placeholder="First Name">
                    <input name="last_name" type="text" placeholder="Last Name">
                    <input name="phone_number" type="text" placeholder="Phone Number">
                    <input name="email" type="text" placeholder="E-mail Address">
                    <input name="company" type="text" placeholder="Company">
                </div>

                <h3>Make Your Account Secure</h3>
                <div class="input-fields">
                    <input name="username" type="text" placeholder="Username">
                    <input name="new_password" type="text" placeholder="Enter Password">
                    <input name="new_password2" type="text" placeholder="Confirm Password">
                </div>
                <br>
                <button type="submit" class="btn btn-lg btn-purple">Start Next Step <i class="glyphicon glyphicon-arrow-right"></i></button>
            </form>
        </div>
        <div class="col-md-4">
            <h2>Commonly Asked Questions</h2>

            <ul class="list-unstyled">
                <li><strong>How much does Bring the Blog cost?</strong> <br />
                    Bring the Blog membership is $97 per month. <br />
                    <br />
                </li>
                <li><strong>What is a content channel?</strong> <br />
                    Channels are exactly what they sound like -- programming for a particular industry. For example, our real estate channel provides real-estate centric blog content. Our mortgage channels provides mortgage-centric blog content. <br />
                    <br />
                </li>
                <li><strong>Which channel is best for me?</strong> <br />
                    Our members often gravitate toward their respective lines of work. You probably should, too. <br />
                    <br />
                </li>
                <li><strong>How long until you start writing for my blog?</strong> <br />
                    We post new blog content every business morning. You'll see your first Bring the Blog blog post shortly. </li>
            </ul>
        </div>
    </div>
</div>
<!-- End #contact -->
