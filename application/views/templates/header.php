<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>Bring the Blog</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/front_main.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/assets/js/shadowbox/shadowbox.css">
    <!-- IE Fix for HTML5 Tags -->
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>

<!-- Navbar -->
<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <a class="brand pull-left" data-section="html" href="#"><img src="/assets/images/bringtheblog-logo-167-45.png" alt=""/></a>

            <div class="navbar">
                <ul class="nav pull-right">
                    <li><a href="/#about" class="scroller" data-section="#about">What We Do</a></li>
                    <li><a href="/#team" class="scroller" data-section="#team">Support</a></li>
                    <li><a href="/sign-up" class="scroller" data-section="#hair">Sign-up</a></li>
	                <? if ($_SESSION['login_state'] == true) { ?>
	                <li><a href="/login/logout" class="scroller" data-section="#contact">Logout</a></li>
	                <? } else { ?>
                    <li><a href="/login" class="scroller" data-section="#contact">Login</a></li>
	                <? } ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- end Navbar -->