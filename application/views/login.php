<div class="main-container">

<!-- #intro -->
<div class="container-fluid top-container">
    <div class="container">
        <div class="col-md-10 col-md-offset-1 text-left">
            <h1 class="georgia text-left">Bring the Blog</h1>
        </div>
    </div>
</div>
<!-- ends Intro -->

<!-- #contact -->
<div class="container-fluid" id="contact">
    <div class="container dark">

	    <?php echo validation_errors('<div class="alert alert-error">', '</div>'); ?>

        <div class="col-md-5 col-md-offset-1">
            <?php echo form_open('login/submit', array('class' => "form-inline registerform", 'style' => "margin: 0px;")); ?>
	            <h3>Member Login</h3>
                <div class="input-fields">
                    <input name="username" class="input-lg" type="text" placeholder="Username">
                    <input name="password" class="input-lg" type="password" placeholder="Enter Password">
                </div>
                <br>
                <button type="submit" class="btn btn-lg btn-purple">Login <i class="glyphicon glyphicon-lock"></i></button>
                <br><br>
            <?php form_close(); ?>
        </div>
        <div class="col-md-4">
            <h3 style="margin-bottom: 35px;">Forget something?</h3>

            <ul class="list-group" style="">
                <li class="list-group-item dark"><a href="#">Reset your password</a></li>
                <li class="list-group-item dark"><a href="#">Recover your username</a></li>
            </ul>
        </div>
    </div>
</div>
<!-- End #contact -->
