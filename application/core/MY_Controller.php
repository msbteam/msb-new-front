<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_Controller extends CI_Controller
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();

        $this->load->helper('url');

        // ensure user is signed in
        if ( $this->session->userdata('login_state') == FALSE ) {
            redirect("/login"); // no session established, kick back to login page
        }
    }
}

class Admin_Controller extends CI_Controller
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();

        $this->load->helper('url');

        // ensure user is signed in
        if ( $this->session->userdata('login_state') == FALSE ) {
            $UID = $this->session->userdata('userid');

            $data = $this->db->select('user_level')->get_where('btb_login_users', array('id' => $UID), 1, 0)->result_array();

            if ($data[0] == 1) {
                redirect("/"); // User account level not high enough.
            }
        } else {
            redirect("/control"); // Not logged in..
        }
    }
}